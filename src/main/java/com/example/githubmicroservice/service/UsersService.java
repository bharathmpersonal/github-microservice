package com.example.githubmicroservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.githubmicroservice.dtos.GithubUsersList;

@Component
public class UsersService {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	GithubUsersList githubUsersList;

	public List<?> loadGithubUsers() {
		// TODO Auto-generated method stub
		return githubUsersList.getGithubUsersList();
	}

}
