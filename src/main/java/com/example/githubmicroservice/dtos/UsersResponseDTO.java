package com.example.githubmicroservice.dtos;

import java.util.List;

public class UsersResponseDTO {

	public String status;
	public String message;
	public List<?> result;
	
	public UsersResponseDTO(String status, String message, List<?> result) {
		super();
		this.status = status;
		this.message = message;
		this.result = result;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<?> getResult() {
		return result;
	}
	public void setResult(List<?> result) {
		this.result = result;
	}
	
}
