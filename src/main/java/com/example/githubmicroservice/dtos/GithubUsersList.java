package com.example.githubmicroservice.dtos;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.ApplicationScope;


@Configuration
@ApplicationScope
public class GithubUsersList {

	List<?> githubUsersList;

	public List<?> getGithubUsersList() {
		return githubUsersList;
	}

	public void setGithubUsersList(List<?> githubUsersList) {
		this.githubUsersList = githubUsersList;
	}


	
}
