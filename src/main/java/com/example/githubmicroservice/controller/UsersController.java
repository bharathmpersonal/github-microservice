package com.example.githubmicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.githubmicroservice.dtos.UsersResponseDTO;
import com.example.githubmicroservice.service.UsersService;
import com.example.githubmicroservice.util.Constants;

@RestController
@RequestMapping("/api/v1")
public class UsersController {

	@Autowired
	UsersService usersService;

	/**
	 * Get all users list.
	 *
	 * @return the list
	 */
	@GetMapping("/github/users")
	public UsersResponseDTO getAllGithubUsers() {
		
		return new UsersResponseDTO(Constants.success,Constants.github_users_fetched_successfully,usersService.loadGithubUsers());
	}
}
