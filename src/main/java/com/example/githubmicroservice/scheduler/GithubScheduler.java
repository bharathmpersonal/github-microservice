package com.example.githubmicroservice.scheduler;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GithubScheduler {

	@Autowired
	GithubService githubService;

	@PostConstruct
	public void onStartup() {
		githubService.loadGithubUsersList();
	}
}
