package com.example.githubmicroservice.scheduler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.githubmicroservice.dtos.GithubUsersDTO;
import com.example.githubmicroservice.dtos.GithubUsersList;

@Component
public class GithubService {

	@Autowired
	GithubUsersList githubUsersList;

	@Autowired
	RestTemplate restTemplate;

	public GithubUsersList loadGithubUsersList() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		int sinceValue = 0;
		List<GithubUsersDTO> githubUsers = new ArrayList<>();

		for (int i = 0; i < 50; i++) {
			ResponseEntity<List<GithubUsersDTO>> response = restTemplate.exchange(
					"https://api.github.com/users?since=" + sinceValue + "&per_page=100", HttpMethod.GET, entity,
					new ParameterizedTypeReference<List<GithubUsersDTO>>() {
					});
			githubUsers.addAll(response.getBody());
			sinceValue = response.getBody().get(99).getId();
		}

		System.out.println(githubUsers);
		githubUsersList.setGithubUsersList((List<?>) githubUsers);

		return githubUsersList;
	}
}
